Dear XXX,

I am a software architect working out of KUKA-Austin, and I have been directed to "look into process simulation." Richard Zunke gave me your name as a potential contact, so I am trying to establish a line of communication, starting with this email.

Although my direction from David Fuller was very general (those four words in quotes above,) I have begun to focus my attention on this question: "Can we provide a tangible benefit to system integrators by augmenting KUKA.Sim with physics-based process simulations?" At Zunke's advice, I have focused my attention on the welding process, in particular. I created a prototype of this idea, which I showed to Fuller when he visited the Austin office recently with Peter Mohnen and Andy Gu. He saw promise in the idea, and encouraged me to pursue it further. That is why I have wound up on your doorstep.
