Discussion with KUKA welding experts
2-5-2019
Charles Adams
Dan Staats
Steven Rudd

Welding process. They support partners/integrators.

All are aware of Octopus. All see that sort of config/simulation as table
stakes in the offline welding world.

Mentioned that almost always the customer requirements are stated in terms
of weld bead geometry. Concavity, convexity, size, etc.

Typically, they have to do destructive testing at the physical cell to validate the
process parameters. For example, cut weld open, look at it under microscope,
make measurements.

They mentioned one customer (non-automotive) that welded large pieces
with a gantry system. For this particular customer, they had certain
weld "schedules" that were known to produce certain weld characteristics.
In this case, the customer did not require any destructive testing.
Rather, they set up the weld in octopus, and then sent it straight
to the robot for the weld.

Charles emailed after the meeting:
> Sorry I wasn’t much help in yesterday’s call, I would like to help
> push this forward with your team and will help with anything your
> team needs. We have a full facility up here with welding equipment
> and welding knowledge so I should be able to help steer you in the
> right direction and help as well
> Keep me posted, thanks!
