import Solver as S
import Slovenia_GMAW as model
import matplotlib.pyplot as plt

current = 0.0 # A
stick_out = 0.12 # m
w_1 = 0.0 # m
w_2 = 0.0 # m
voltage = 24.0 # V
torch_speed = 0.05 # m/s 
electrode_workpiece_distance = 0.016 # m

solver_dt = 5e-4

x0 = [current, stick_out, w_1, w_2]
u0 = [voltage,torch_speed,electrode_workpiece_distance]

s = S.Solver(x0,u0,0.0,solver_dt,model,"BackwardEuler")
res = s.Simulate(11.9)

labels = ["current","stick-out","bead width"]
#fig, ax = plt.subplots()
#for i in [2]:
#    plt.subplot(1,1,i+1)
#    plt.plot(res.T, [y[i] for y in res.Y],label = labels[i])
#    plt.title(labels[i])
#legend = ax.legend(loc='lower right', shadow=True)
plt.figure(figsize=[9,7])
plt.subplot(3,1,1)
plt.plot(res.T, [y[2] for y in res.Y])
title = 'Bead width with changing tool to workpiece distance'
plt.title(title)
#plt.xlabel('time (s)')
plt.ylabel('bead width (m)')
plt.subplot(3,1,2)
plt.plot(res.T, [y[3] for y in res.Y])
#plt.xlabel('time (s)')
plt.ylabel('tool dist. (m)')
plt.subplot(3,1,3)

w = []
t = []
yy = [y[2] for y in res.Y]
i = 0
for yyy in yy:
    w.append(-yyy/2)
    w.append(yyy/2)
    t.append(res.T[i])
    t.append(res.T[i])
    i += 1
plt.plot(t, w)
plt.xlabel('time (s)')
plt.ylabel('weld geometry (m)')
#plt.ylim([0,.020])
plt.savefig(title+'.png')
plt.show()
