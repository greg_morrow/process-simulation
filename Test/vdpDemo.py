import Solver as S
import VanDerPol as model
import matplotlib.pyplot as P

ms = [0.1,0.2,0.5,1.0,2.0,5.0]

x = []
fig, ax = P.subplots()
for i in range(len(ms)):
    model.m = ms[i]
    s = S.Solver([1,0],[0,0],0,0.1,model,"BackwardEuler")
    print "m = " + str(ms[i])
    s.Simulate(10)
    res = s.Simulate(1000)
    #s.Stats()
    ax.plot([y[0] for y in res.Y], [y[1] for y in res.Y],label = "m = " + str(ms[i]))

#P.plot(x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9],x[10],x[11])
legend = ax.legend(loc='upper left', shadow=True)
#ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
# Put a nicer background color on the legend.
legend.get_frame().set_facecolor('#00FFCC')
P.title("van der Pol oscillator")
P.show()