import Solver as S
import ExponentialDecay as model
import matplotlib.pyplot as P

s = S.Solver([1],[0],0,0.1,model)
res = s.Simulate(10)
s.Stats()
P.plot(res.T,res.Y)
P.show()
