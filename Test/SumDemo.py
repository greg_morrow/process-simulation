import Solver as S

k = S.KahanSum()

dx = 0.1
s = 0.0
iter = 10000000
for i in range(iter):
    s = s + dx
    ss = k.Add(dx)
    
print ('After {} iterations, s={:16.16%}, ss={:16.16%}'.format(iter, s, ss))