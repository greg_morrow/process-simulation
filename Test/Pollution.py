import Solver as S

# constants
k = S.np.zeros(25)
k[0] = 0.35
k[1] = 0.266E2
k[2] = 0.123E5
k[3] = 0.860E-3
k[4] = 0.820E-3
k[5] = 0.150E5
k[6] = 0.130E-3
k[7] = 0.240E5
k[8] = 0.165E5
k[9] = 0.9E4
k[10] = 0.220E-1
k[11] = 0.120E5
k[12] = 0.188E1
k[13] = 0.163E5
k[14] = 0.480E7
k[15] = 0.350E-3
k[16] = 0.175E-1
k[17] = 0.100E9
k[18] = 0.444E12
k[19] = 0.124E4
k[20] = 0.210E1
k[21] = 0.578E1
k[22] = 0.474E-1
k[23] = 0.178E4
k[24] = 0.312E1

def r(x):
    # fill in the r's from table II.2.1

def f(x,u,t):
    # fill in the x's based on r's
    return S.np.array([a*xx for xx in x])
    
def g(x,u,t):
    return x
    
def e(x,x_old,t):
    thresh = 0.0
    if ((x[0]-thresh)*(x_old[0]-thresh)<=0):
        result = True
    else:
        result = False
        
    # reset the states to [1]
    return S.ResetResult(result, S.np.array([1]))
    
def jac(x,u,t):
    return S.np.array([a])
        
