import Solver as S
m = 6
def f(xx,u,t):
    x = xx[0]
    v = xx[1]
    return S.np.array([v, m*(1.0-x*x)*v - x])
    
def g(x,u,t):
    return x
       
# no state reset
def e(x,x_old,t):
    return S.ResetResult(False, x)
    
def jac(x,u,t):
    j = S.np.zeros((2,2))
    j[0,0] = 0.0
    j[0,1] = 1.0
    j[1,0] = -2*x[0]*x[1]-1
    j[1,1] = m*(1-x[0]*x[0])
    return j