import Solver as S
import BouncingBall as B
import matplotlib.pyplot as P

s = S.Solver([10,0],[0,0],0,0.1,B)
res = s.Simulate(10)

P.plot(res.T, res.Y)
P.show()
