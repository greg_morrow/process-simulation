import numpy as np

def Euler(solver):
    solver.subSteps += 1
    return solver.x + np.dot(solver.dt, solver.f(solver.x,solver.u,solver.t))
    
def BackwardEuler(solver):
    max_steps = 100
    tolerance = 0.0001
    x_len = len(solver.x)
    temp = np.zeros(x_len)
    done = False
    step = 0
    x1 = solver.x
    x_start = x1
    I = np.eye(x_len)
    while (not done):
        step += 1
        x0 = x1
        xt = x0-np.dot(solver.f(x0,solver.u,solver.t),solver.dt)-x_start
        dF = I-np.dot(solver.dt, solver.jac(x0, solver.u, solver.t))
        x1 = x0-np.linalg.solve(dF, xt)
        done = (np.linalg.norm(x1-x0) < 0.0001) and (step < max_steps)
    if step >= max_steps:
        print "Too many steps BackwardsEuler"
        
    if (step > solver.maxSubSteps):
        solver.maxSubSteps = step
    solver.subSteps += step
    return x1    
    
    
class KahanSum:
    def __init__(self):
        self.sum = 0.0
        self.c = 0.0
    def Add(self, x):
        y = x - self.c
        t = self.sum + y
        self.c = (t-self.sum)-y
        self.sum = t
        return self.sum
    def Reset(self, s):
        self.c = 0.0
        self.sum = s
        return self.sum
    
class Solver:
    def __init__(self,x0,u0,t0,dt,model,odeType="Euler"):
        self.x = np.array(x0)
        self.x0 = np.array(x0)
        self.t = t0
        self.t0 = t0
        self.dt = dt
        self.u = np.array(u0)
        self.u0 = np.array(u0)
        self.f = model.f
        self.g = model.g
        self.e = model.e
        self.jac = model.jac
        self.y = self.g(self.x, self.u, self.t)
        self.subSteps = 0
        self.steps = 0
        self.maxSubSteps = 0
        self.KSum = KahanSum()
        if (odeType=="Euler"):
            self.solver = Euler
        else:
            self.solver = BackwardEuler

    def Simulate(self,tfinal):
        res = Solution([self.t],[self.y])
        while self.t < tfinal:
            #print "[ {} ]:{}".format(self.t, self.x)
            y = self.Advance(self.u0) # note uses u0...
            res.append(self.t, y)
            
        return res
      
    def Advance(self,u):
        self.u = u
        temp = self.solver(self)
        resetResult = self.e(temp, self.x, self.t)
        if resetResult.result:
            print "Reset states = {} time = {}".format(resetResult.states,self.t)
            self.Reset(resetResult.states)
        else:
            self.x = temp
        self.t = self.KSum.Add(self.dt)
        self.y = self.g(self.x,u,self.t)
        self.steps += 1
        return self.y
        
        
    def Reset(self, states):
        self.x = states
        #self.u = self.u0
        self.y = self.g(self.x,self.u,self.t)
 
    def ResetAll(self):
        self.t = self.KSum.Reset(self.t0)
        self.Reset(self.x0)
       
    def Stats(self):
        print "Number of steps = " + str(self.steps)
        print "Number of subSteps = " + str(self.subSteps)
        print "Max subSteps = " + str(self.maxSubSteps)
        
class ResetResult:
    def __init__(self, result, states):
        self.result = result
        self.states = states
        
class Solution:
    def __init__(self, ts, ys):
        self.T = ts
        self.Y = ys
        self.steps = 0
        self.subSteps = 0
        
    def append(self,t,y):
        self.T.append(t)
        self.Y.append(y)
       

class ODEModel:
    def __init__(self,f,g,e,jac):
        self.f = f
        self.g = g
        self.e = e
        self.jac = jac