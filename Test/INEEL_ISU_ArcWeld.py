# INEEL/ISU welding droplet model
# see "Modeling Sensing and Control of Arc Welding Process" S. Ozcelik
#
#
import Solver as S
import math as math
a = 0.641
b = 1.15
c = 0.196
C_1 = 2.8855e-10
C_2 = 5.22e-10
C_d = 0.44
rho = 0.2821
rho_w = 7860
rho_p = 1.6
K = 2.5
B = 1e-5
r_w = 4.445e-4
L_s = 0.14e-3
R_a = 0.022
R_s = 0.004
V_0 = 15.7
E_a = 1500
U_b = 10.0
gamma = 2
mu_0 = 1.25664e-6
pi = math.pi

# so these will be global to this file
M_r = 0.0
r_d = 0.0
R_L = 0.0
F_em = 0.0
F_d = 0.0
F_m = 0.0
F_g = 0.0
F_tot = 0.0

def evalEqns(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    xd = S.np.zeros(5)
    CT = u3
    M_r = C_2*rho*x4*x5*x5+C_1*x5
    r_d = pow(3.0*math.fabs(x3)/(4*pi*rho_w),1.0/3.0)
    R_L = rho*(x4+0.5*(x1+r_d))
    F_em = (mu_0*x5*x5/(4*pi))*(a/(1+math.exp((b-(r_d/r_w))/c)))
    F_d = (C_d*(r_d*r_d-r_w*r_w)*pi*rho_p*U_b*U_b)/2.0
    F_m = M_r*rho_w*u1
    F_g = 9.81*x3
    F_tot = F_em + F_d + F_m + F_g
    

# x1 droplet displacement
# x2 droplet velocity
# x3 droplet mass
# x4 stick-out
# x5 current
# u1 wire-feed speed
# u2 open-circuit voltage V_oc
# u3 contact tip-to-workpiece distance
def f(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    xd = S.np.zeros(5)
    evalEqns(x,u,t)
    xd[0] = x2
    xd[1] = (-K*x1 - B*x2 + F_tot)/x3
    xd[2] = M_r*rho_w
    xd[3] = u1 - M_r/(math.pi*r_w*r_w)
    xd[4] = (1.0/L_s)*(u2-(R_a+R_s+R_L)*x5-V_0-E_a*(u3-x4))
    return xd

def jac(x,u,t):
    j = S.np.zeros([5,5])
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    evalEqns(x,u,t)
    j[0,1] = 1.0
    j[1,0] = -K/x3
    j[1,1] = -B/x3
    j[1,2] = -(-K*x1 - B*x2 + F_tot)/(x3*x3)*(9.81+C_d*r_d*(rho_p/rho_w)*U_b*U_b*math.pow(3*x3/(4*pi*rho_w),2.0/3.0))
    j[1,3] = rho_w*u1*C_2*rho*x5*x5/x3
    j[1,4] = (1.0/x3)*(((mu_0*x5/(2*pi))*(a/(1+math.exp((b-(r_d/r_w))/c))))+rho_w*u1*(2*C_2*rho*x4*x5+C_1))
    j[2,3] = rho_w*(C_2*rho*x5*x5)
    j[2,4] = rho_w*(C_2*rho*2*x4*x5+C_1)
    j[3,3] = -(1.0/pi*r_w*r_w)*(C_2*rho*x5*x5)
    j[3,4] = -(1.0/pi*r_w*r_w)*(C_2*rho*2*x4*x5+C_1)
    j[4,0] = (-1.0/(2.0*L_s))*x5*rho
    j[4,2] = (rho/(8.0*L_s*pi*rho_w))*math.pow(3*x3/(4.0*pi*rho_w),2.0/3.0)
    j[4,3] = (E_a-x5*rho)/L_s
    j[4,4] = -(R_a+R_s+R_L)/L_s
    return j
    
   
# y1 arc voltage
# y2 arc current
def g(x,u,t):
    y = S.np.zeros(6)
    y[0] = V_0 + R_a*x[4]+E_a*(u[2]-x[3])
    y[1] = x[4]
    y[2] = x[0]
    y[3] = x[1]
    y[4] = x[2]
    y[5] = x[3]
    return y
    
def e(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    CT = u3
    evalEqns(x,u,t)
    F_s = 2.0*pi*gamma*r_d
    r_s = r_d + r_w
    r_c = pi*r_s/(1.25*((x1+r_d)/r_d)*math.sqrt(1+mu_0*x5*x5/1*pi*pi*gamma*r_s))
    dF = F_tot-F_s
    dr = r_d - r_c
    print "F_tot-F_s = {}, r_d-r_c = {}".format(dF,dr)
    if (dF>0.0 or dr>0.0):
        result = True
        # set up the new states
        xnew = S.np.zeros(5)
        xnew[0] = r_d
        xnew[1] = 0
        xnew[2] = (x3/2.0)#*(1.0-1.0/(1.0+math.exp(-100.0*x2)))
        xnew[3] = x4
        r_d_new = pow(3.0*xnew[2]/(4*pi*rho_w),1.0/3.0)
        R_L_new = rho*(x4+0.5*(r_d_new))
        xnew[4] = x5+(u2-V_0-E_a*(u3-x4))/(R_a+R_s+R_L_new)-(u2-V_0-E_a*(u3-x4))/(R_a+R_s+R_L)
    else:
        result = False
        xnew = x
        
    return S.ResetResult(result, xnew)
    
    
    