import Solver as S
a = -1.0
def f(x,u,t):
    return S.np.array([a*xx for xx in x])
    
def g(x,u,t):
    return x
    
def e(x,x_old,t):
    thresh = 0.0
    if ((x[0]-thresh)*(x_old[0]-thresh)<=0):
        result = True
    else:
        result = False
        
    # reset the states to [1]
    return S.ResetResult(result, S.np.array([1]))
    
def jac(x,u,t):
    return S.np.array([a])
        
