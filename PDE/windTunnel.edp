//
// Solve the Navier-Stokes problem on a rectangular domain with an airfoil-shaped section removed from the middle.
//
real hy = 1, hx = 6;
real flowRate = 2;
real dt=0.03;
real tFinal = 1;
real angleOfAttackCoefficient=-0.12;


// The wind tunnel 
border Bottom(t=0,1)    {x=hx*t;                y=0;                label=1;}
border Right(t=0,1)     {x=hx;                  y=hy*t;             label=2;}
border Top(t=1,0)       {x=hx*t;                y=hy;               label=3;}
border Left(t=1,0)      {x=0;                   y=hy*t;             label=4;}

// the airfoil
real x0=hx/8, y0=hy/2;
border Splus(t=0,1) {x=x0+t;y = angleOfAttackCoefficient*t+y0+0.17735*sqrt(t)-0.075597*t-0.212836*(t^2)+0.17363*(t^3)-0.06254*(t^4);label=5;}
border Sminus(t=0,1) {x=x0+t;y = angleOfAttackCoefficient*t+y0-0.5*(0.17735*sqrt(t)-0.075597*t-0.212836*(t^2)+0.17363*(t^3)-0.06254*(t^4));label=5;}


int scale = 30; // sets the number of elements per border element
int sideScale=scale*(hx/hy);
mesh Th = buildmesh(Bottom(sideScale)+Right(scale)+Top(sideScale)+Left(scale)+Sminus(-3*scale)+Splus(3*scale));
plot(Th, wait=true);

fespace Xh(Th,P2);
fespace Mh(Th,P1);
Xh u2, v2;
Xh u1, v1;
Mh p,q;
solve Stokes (u1,u2,p,v1,v2,q,solver=Crout) =
    int2d(Th)( ( dx(u1)*dx(v1) + dy(u1)*dy(v1)
    + dx(u2)*dx(v2) + dy(u2)*dy(v2) )
    - p*q*(0.000001)
    - p*dx(v1) - p*dy(v2)
    - dx(u1)*q - dy(u2)*q
    )
    + on(4,u1=flowRate,u2=0)
    //+ on(2,u1=flowRate,u2=0)
    + on(1,3,5,u1=0,u2=0);
Xh psi,phi;
solve streamlines(psi,phi) =
    int2d(Th)( dx(psi)*dx(phi) + dy(psi)*dy(phi))
    + int2d(Th)( -phi*(dy(u1)-dx(u2)))
    + on(1,2,3,4,5,psi=0);
int i=0;
real nu=1./1000.;
real alpha=1/dt;
int nSteps = tFinal/dt;
Xh up1,up2;
problem NS (u1,u2,p,v1,v2,q,solver=Crout,init=i) =
    int2d(Th)(
    alpha*( u1*v1 + u2*v2)
    + nu * ( dx(u1)*dx(v1) + dy(u1)*dy(v1)
    + dx(u2)*dx(v2) + dy(u2)*dy(v2) )
    - p*q*(0.000001)
    - p*dx(v1) - p*dy(v2)
    - dx(u1)*q - dy(u2)*q
    )
    + int2d(Th) ( -alpha*
    convect([up1,up2],-dt,up1)*v1 -alpha*convect([up1,up2],-dt,up2)*v2 )
    + on(4,u1=flowRate,u2=0)
    + on(2,u1=flowRate,u2=0)
    + on(1,3,5,u1=0,u2=0);
    ;
real t=0;
for (i=0;i<=nSteps;i++)
    {
    up1=u1;
    up2=u2;
    NS;
    if ( !(i % 1)) // plot every 10 iteration
    //plot(coef=0.2,cmm=" [u1,u2], t=" + t,[u1,u2],prev=true,bb=[[x0-0.1,y0-0.1],[x0+3,y0+0.5]]);
    plot(coef=0.2,cmm=" [u1,u2], t=" + t,[u1,u2],prev=true);
    t = t + dt;
    } ;
real px = int1d(Th,Splus)(N.x*p)+int1d(Th,Sminus)(N.x*p);
real py = int1d(Th,Splus)(N.y*p)+int1d(Th,Sminus)(N.y*p);
cout << "lift= [" + px + "," +  py + "]" << endl;
plot(coef=0.2,cmm=" p, t=" + t, p,nbiso=50,bb=[[x0-0.1,y0-0.1],[x0+1.3,y0+0.5]]);
