# find the eigenmodes of a circular drum head
# try to solve the eigenproblem: del u = lambda u
import numpy as np
import matplotlib.pyplot as pp

R = 1
Nx = 101
Ny = 101

xx = np.linspace(-R,R,num=Nx)
yy = np.linspace(-R,R,num=Ny)

u = []
du = []

for y in yy:
    first_x = -1
    last_x = -1
    inRegion = False
    for x in xx:
        if (not inRegion):
            if (x*x+y*y<=R*R):
                inRegion = True
                # note this is the first x in this row to be in the domain
                first_x = x
                u.append([x,y])
        else: # inRegion == True
            if (x*x+y*y>R):
                inRegion = False
                last_x = x
            else:
                u.append([x,y])
    du.append([first_x,y])
    du.append([last_x,y])

for x in xx:
    first_y = -1
    last_y = -1
    inRegion = False
    for y in yy:
        if (not inRegion):
            if (x*x+y*y<=R*R):
                inRegion = True
                # note this is the first x in this row to be in the domain
                first_y = y
        else: # inRegion == True
            if (x*x+y*y>R):
                inRegion = False
                last_y = y
    try:
        i = du.index([x,first_y])
    except ValueError:
        du.append([x,first_y])
    try:
        i = du.index([x,last_y])
    except ValueError:
        du.append([x,last_y])




pp.scatter([uu[0] for uu in u],[uu[1] for uu in u])
pp.scatter([uu[0] for uu in du],[uu[1] for uu in du])
pp.show()
