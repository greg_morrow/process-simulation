import Solver as S
import Spot2 as model
import matplotlib.pyplot as P
import Control

pi = Control.PI_Controller(20, 0.1, 20000, 0)

# weld for a short duration
# during the weld, use PI control for the input current, controlling to a temperature
def inputs(t, y):
    if (t > 0.03):
        value = 0.0
    else:
        sp = 1900
        value = pi.control(y[0], sp, t)
    return [value]

dt = 1e-5
s = S.Solver([300],[0],0,dt, model, inputs)

result = s.Simulate(1)

P.figure(figsize = [13,9])
P.suptitle("Spot Welding Process--Nugget Temperature and Welding Current")
P.subplot(2,1,1)
P.ylabel("Temperature (K)")
P.plot(result.T, [y[0] for y in result.Y])
P.subplot(2,1,2)
P.ylabel("Welding Current (kA)")
P.plot(result.T, [y[1]/1000 for y in result.Y])
P.show()
