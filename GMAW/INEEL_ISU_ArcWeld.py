# INEEL/ISU welding droplet model
# see "Modeling Sensing and Control of Arc Welding Process" S. Ozcelik
#
#
import Solver as S
import math as math
a = 0.641
b = 1.15
c = 0.196
C_1 = 2.8855e-10
C_2 = 5.22e-10
C_d = 0.44
rho = 0.2821
rho_w = 7860
rho_p = 1.6
K = 2.5
B = 1e-5
r_w = 4.445e-4
L_s = 0.14e-3
R_a = 0.022
R_s = 0.004
V_0 = 15.7
E_a = 1500
U_b = 10.0
gamma = 2
mu_0 = 1.25664e-6
pi = math.pi

class INEEL_Eqn:
    def __init__(self):
        self.M_r = 0.0
        self.r_d = 0.0
        self.R_L = 0.0
        self.F_em = 0.0
        self.F_d = 0.0
        self. F_m = 0.0
        self.F_g = 0.0
        self.F_tot = 0.0

def evalEqns(x,u,t):
    eq = INEEL_Eqn()
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    CT = u3
    eq.M_r = C_2*rho*x4*x5*x5+C_1*x5
    eq.r_d = pow(3.0*math.fabs(x3)/(4*pi*rho_w),1.0/3.0)
    eq.R_L = rho*(x4+0.5*(x1+eq.r_d))
    eq.F_em = (mu_0*x5*x5/(4*pi))*(a/(1+math.exp((b-(eq.r_d/r_w))/c)))
    eq.F_d = (C_d*(eq.r_d*eq.r_d-r_w*r_w)*pi*rho_p*U_b*U_b)/2.0
    eq.F_m = eq.M_r*rho_w*u1
    eq.F_g = 9.81*x3
    eq.F_tot = eq.F_em + eq.F_d + eq.F_m + eq.F_g
    return eq

# x1 droplet displacement
# x2 droplet velocity
# x3 droplet mass
# x4 stick-out
# x5 current
# x6 amount of mass transferred
# u1 wire-feed speed
# u2 open-circuit voltage V_oc
# u3 contact tip-to-workpiece distance
def f(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    xd = [0,0,0,0,0,0]
    eq = evalEqns(x,u,t)
    xd[0] = x2
    xd[1] = (-K*x1 - B*x2 + eq.F_tot)/x3
    xd[2] = eq.M_r*rho_w
    xd[3] = u1 - eq.M_r/(math.pi*r_w*r_w)
    xd[4] = (1.0/L_s)*(u2-(R_a+R_s+eq.R_L)*x5-V_0-E_a*(u3-x4))
    xd[5] = 0.0 # mass transfer is handled in reset function
    return xd

def jac(x,u,t):
    row = [0,0,0,0,0,0]
    j = [r,r,r,r,r,r]
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    eq = evalEqns(x,u,t)
    j[0,1] = 1.0
    j[1,0] = -K/x3
    j[1,1] = -B/x3
    j[1,2] = -(-K*x1 - B*x2 + eq.F_tot)/(x3*x3)*(9.81+C_d*eq.r_d*(rho_p/rho_w)*U_b*U_b*math.pow(3*x3/(4*pi*rho_w),2.0/3.0))
    j[1,3] = rho_w*u1*C_2*rho*x5*x5/x3
    j[1,4] = (1.0/x3)*(((mu_0*x5/(2*pi))*(a/(1+math.exp((b-(eq.r_d/r_w))/c))))+rho_w*u1*(2*C_2*rho*x4*x5+C_1))
    j[2,3] = rho_w*(C_2*rho*x5*x5)
    j[2,4] = rho_w*(C_2*rho*2*x4*x5+C_1)
    j[3,3] = -(1.0/pi*r_w*r_w)*(C_2*rho*x5*x5)
    j[3,4] = -(1.0/pi*r_w*r_w)*(C_2*rho*2*x4*x5+C_1)
    j[4,0] = (-1.0/(2.0*L_s))*x5*rho
    j[4,2] = (rho/(8.0*L_s*pi*rho_w))*math.pow(3*x3/(4.0*pi*rho_w),2.0/3.0)
    j[4,3] = (E_a-x5*rho)/L_s
    j[4,4] = -(R_a+R_s+eq.R_L)/L_s
    return j


# [0] y1 arc voltage
# [1] y2 arc current
# [2] droplet position
# [3] droplet velocity
# [4] droplet mass
# [5] electrode stick-out
# [6] wire feed speed
# [7] supply voltage
# [8] tip-to-workpiece
# [9] mass transferred
# [10] arc power
def g(x,u,t):
    y = [0,0,0,0,0,0,0,0,0,0,0]
    y[0] = V_0 + R_a*x[4]+E_a*(u[2]-x[3])
    y[1] = x[4]
    y[2] = x[0]
    y[3] = x[1]
    y[4] = x[2]
    y[5] = x[3]
    y[6] = u[0]
    y[7] = u[1]
    y[8] = u[2]
    y[9] = x[5]
    y[10] = abs(x[4]*y[0])
    return y

def e(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = x[2]
    x4 = x[3]
    x5 = x[4]
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    CT = u3
    eq = evalEqns(x,u,t)
    F_s = 2.0*pi*gamma*eq.r_d
    r_s = eq.r_d + r_w
    r_c = pi*r_s/(1.25*((x1+eq.r_d)/eq.r_d)*math.sqrt(1+mu_0*x5*x5/1*pi*pi*gamma*r_s))
    dF = eq.F_tot-F_s
    dr = eq.r_d - r_c
    if (dF>0.0 or dr>0.0):
        if S.DebugPrint:
            print "[{}]: F_tot-F_s = {}, r_d-r_c = {}".format(t,dF,dr)
        result = True
        # set up the new states
        xnew = [0,0,0,0,0,0]
        xnew[0] = eq.r_d
        xnew[1] = 0
        xnew[2] = (x3/2.0)#*(1.0-1.0/(1.0+math.exp(-100.0*x2)))
        xnew[3] = x4
        r_d_new = pow(3.0*xnew[2]/(4*pi*rho_w),1.0/3.0)
        R_L_new = rho*(x4+0.5*(r_d_new))
        xnew[4] = x5+(u2-V_0-E_a*(u3-x4))/(R_a+R_s+R_L_new)-(u2-V_0-E_a*(u3-x4))/(R_a+R_s+eq.R_L)
        # add the current droplet mass to the "mass transferred" pseudostate
        xnew[5] = x[5]+x[2]
    else:
        result = False
        xnew = x

    return S.ResetResult(result, xnew)
