import Solver as S
import INEEL_ISU_ArcWeld as model
import matplotlib.pyplot as plt

# initial states
x0 = [0.001,0.001,0.001,0.0,1,0]

# function to vary model inputs over time
def inputs(t,y):
    t_warm = 0
    u = [0,0,0]
    if (t>t_warm):
        ti = int(0.2*(t-t_warm))
    else:
        ti = 0
    # impulse in wire feed speed
    '''
    if (4.9 < t and t < 5.1):
        ti = 1.0
    else:
        ti = 0.0
    '''
    u[0] = 0.15 + 0.15*ti
    u[1] = 29
    u[2] = .016 #+ 0.004*ti
    return u

solver_dt = 1.0e-4
s = S.Solver(x0,inputs(0,[]),0.0,solver_dt,model,inputs)

res = s.Simulate(10)

labels = ["arc voltage","current","droplet displacement","droplet velocity","droplet mass","stick-out","wire feed speed (controlled)","open circuit voltage","tip-to-workpiece","electrode mass transferred","power"]
toPlot = [0,1,2,4,5,9,10,6]
plotRows = 4
plotCols = 2
fig, ax = plt.subplots(figsize=[13,9])
for plotIndex in range(len(toPlot)):
    i = toPlot[plotIndex]
    plt.subplot(plotRows,plotCols,plotIndex+1)
    plt.plot(res.T, [y[i] for y in res.Y],label = labels[i])
    plt.title(labels[i],y=0.5)
    plt.xlim([0,10])
legend = ax.legend(loc='lower right', shadow=True)
title = "INEEL_ISU effect of changing wire feed speed"
plt.suptitle(title)
plt.savefig(title+".png")
plt.show()
