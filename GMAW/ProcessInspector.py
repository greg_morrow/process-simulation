class ProcessInspector:
    def __init__(self, criteria):
        self.Criteria = criteria

    def inspect(self, y, t):
        for criterion in self.Criteria:
            criterion.inspect(y,t)

    def inspectAll(self, solution):
        for i in range(len(solution.T)):
            y = solution.Y[i]
            t = solution.T[i]
            self.inspect(y,t)


class ProcessInspectionCriterion:
    def __init__(self, index, ranges, messages):
        if (len(ranges) != len(messages)):
            print "Mismatched ranges and messages in ProcessInspectionCriterion creation"
        self.ranges = ranges
        self.messages = messages
        self.numRanges = len(ranges)
        self.index = index
        self.inside = [0]*self.numRanges

    def inspect(self, Y, t):
        y = Y[self.index]
        for i in range (self.numRanges):
            r = self.ranges[i]
            if (self.inside[i] == 0):
                if (y >= r[0] and y < r[1]):
                    print "[{}] {}".format(t, self.messages[i])
                    self.inside[i] = 1
            else: # self.inside[i] == 1
                if (y < r[0] or y >= r[1]):
                    self.inside[i] = 0
