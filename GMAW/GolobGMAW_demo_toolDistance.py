import Solver as S
import Golob_GMAW as model
import matplotlib.pyplot as plt

current = 0.0 # A
stick_out = 0.0 # m
voltage = 24.0 # V
torch_speed = 0.05 # m/s 
electrode_workpiece_distance = 0.016 # m

solver_dt = 5e-6

# 0 -> Voltage
# 1 -> torch speed
# 2 -> electrode to workpiece distance
def inputs(t):
    inp = S.np.zeros(3)
    inp[0] = 24 # V
    ti = int(0.25*t)
    inp[1] = .001 #+ ti*0.1 # m/s
    inp[2] = 0.0  # 25 + 0.010*ti # m
    return inp

x0 = [current, stick_out]
u0 = inputs(0)

s = S.Solver(x0,u0,0.0,solver_dt,model,inputs,"BackwardEuler")
res = s.Simulate(5.0)

# create plots
title = 'Current'
#plt.title(title)
f = plt.figure(figsize=[13,6])
f.suptitle(title)
plt.plot(res.T,res.Y)
plt.savefig(title+'.png')
plt.show()
