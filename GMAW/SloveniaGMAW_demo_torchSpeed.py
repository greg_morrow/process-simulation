import Solver as S
import Slovenia_GMAW as model
import matplotlib.pyplot as plt
import ProcessInspector as PI

current = 0.0 # A
stick_out = 0.01 # m
w_1 = 0.0 # m
w_2 = 0.0 # m
voltage = 24.0 # V
torch_speed = 0.05 # m/s
electrode_workpiece_distance = 0.016 # m

solver_dt = 1e-5

# 0 -> Voltage
# 1 -> torch speed
# 2 -> electrode to workpiece distance
def inputs(t, y):
    inp = [0.0, 0.0, 0.0, 0.0] #S.np.zeros(4)
    inp[0] = 24 # V
    ti = int(0.33*t)
    # this is a "pulse" of slower torch speed from 5 to 5.3
    if (t<5 or t>5.3):
        inp[1] = .02 #+ ti*0.01# m/s
    else:
        inp[1] = 0.002
    inp[2] = 0.05 #+ 0.010*ti # m
    inp[3] = 0.005
    return inp

x0 = [current, stick_out, w_1, w_2]
u0 = inputs(0,[])

s = S.Solver(x0,u0,0.0,solver_dt,model,inputs)
res = s.Simulate(11.9)

pic = PI.ProcessInspectionCriterion(2, [[0.003,0.004],[0.004,1.0]], ["Weld bead warning: too large","Weld burn-through error"])
pi = PI.ProcessInspector([pic])
pi.inspectAll(res)

# create plots
title = 'Bead width with changing torch speed'
#plt.title(title)
f = plt.figure(figsize=[13,6])
f.suptitle(title)
plt.subplot(2,2,1)
plt.plot(res.T, [y[2] for y in res.Y])
plt.ylabel('bead width (m)')
plt.subplot(2,2,3)
plt.plot(res.T, [y[4] for y in res.Y])
plt.xlabel('time (s)')
plt.ylabel('torch speed (m/s)')
plt.subplot(2,2,2)
# create visualization of weld geometry
w = []
t = []
yy = [y[2] for y in res.Y]
v = [y[4] for y in res.Y]
i = 0
# for each y value, make two points, {res.T[i],-y/2} and {res.T[i],y/2}
for y in yy:
    w.append(-y/2)
    w.append(y/2)
    if (i > 0):
        p += (res.T[i]-res.T[i-1])*v[i]
    else:
        p = 0.0
    t.append(p)
    t.append(p)
    i += 1
plt.plot(t, w)
plt.xlabel('position on workpiece (m)')
plt.ylabel('weld geometry (m)')
plt.ylim([-0.05,0.05])
plt.savefig(title+'.png')
plt.show()
