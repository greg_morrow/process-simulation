import Solver as S
import math
# Model of the GMAW beam geometry.
#
#
R = 0.07 # Ohm
R_k = 0.01 # Ohm
L = 0.02e-3 # H
l_0 = 10e-3 # m
rho = 1.0e-6 # Ohm * m
A = 1.02e-6 # m^2
E = 0.675e3 # V/m
v_e = 85.0e-3 # m/s
K_1 = 2.26e-3 # m/(A*s)
K_2 = 7.55e-5 # 1/(A^2 s)
v_0 = 0.0 #8.3e-4 # m/sec
kappa_1 = 0.2
tau_1 = 0.02 # s
c = 0.0 # m
kappa_2 = 0.12e-3 # m/(A*s)
tau_2 = 0.15 # s
omega = 200 # Hz (freq of droplet production)

def droplet(t):
    # droplet production square wave
    if (math.sin(2.0*math.pi*omega*t)>0.0):
        d = 1.0
    else:
        d = 0.0
    return d

# x[0] = i (current)
# x[1] = l (stick-out)
# x[2] = w_1 (first component of bead width)
# x[3] = w_2 (second component of bead width)
# u[0] = V (open circuit voltage)
# u[1] = v (torch speed)
# u[2] = H (electrode to workpiece distance)
# u[3] = v_e (wire feed speed)
def f(x,u,t):
    i = x[0]
    l = x[1]
    w_1 = x[2]
    w_2 = x[3]
    V = u[0]
    v = u[1]
    H = u[2]
    v_e = u[3]
    d = droplet(t)
    xd = [0.0, 0.0, 0.0, 0.0] #S.np.zeros(4)
    xd[0] = (1/L)*(V-R*i-rho*l*i/A-((H-l)*(E*(1.0-d)+i*R_k*d)))
    xd[1] = v_e - K_1*i - K_2*i*i*l
    xd[2] = -w_1/tau_1 - kappa_1*(v-v_0)/tau_1
    xd[3] = (kappa_2*i-w_2)/tau_2 - c/tau_2
    return xd

# y[0] = i (current)
# y[1] = l (stick-out)
# y[2] = weld bead width
# y[3] = tool to workpiece distance
# y[4] = torch speed
# y[5] = wire feed speed
def g(x,u,t):
    y = [0.0,0.0,0.0,0.0,0.0,0.0] #S.np.zeros(6)
    y[0] = x[0]
    y[1] = x[1]
    y[2] = x[2]+x[3]
    y[3] = u[2]
    y[4] = u[1]
    y[5] = u[3]
    return y

# no resets in this model
def e(x,u,t):
    return S.ResetResult(False, None)

# compute the jacobian jac[i,j] = df_i/dx_j
def jac(x,u,t):
    i = x[0]
    l = x[1]
    w_1 = x[2]
    w_2 = x[3]
    uu = u
    V = uu[0]
    v = uu[1]
    H = uu[2]
    d = droplet(t)
    j = [[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0]]
    #S.np.zeros([4,4])
    j[0,0] = (1/L)*(-R-rho*l/A)
    j[0,1] = (1/L)*(-rho*i/A+(E*(1.0-d)+R_k*d))
    j[1,0] = -K_1 -2*K_2*i*l
    j[1,1] = -K_2*i*i
    j[2,2] = -1.0/tau_1
    j[3,0] = kappa_2/tau_2
    j[3,3] = -1.0/tau_2
    return j
