# a first order system
import Solver as S

def f(x,u,t):
    k = -1
    return [k*x[0] + u[0]]

def g(x,u,t):
    return [x[0],u[0]]

def e(x,u,t):
    return S.ResetResult(False, [])

def jac(x,u,t):
    pass
