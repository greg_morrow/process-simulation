import Solver as S
import math

R = 0.07 # Ohm
L = 0.02e-3 # H
rho = 1.0 # Ohm/m
A = 1.02e-6 # m^2
E = 675 # V/m
u_ac = 11.55 # V
R_arc = 0.03 # Ohm
k1 = 0.000626 # m/(A s)
k2 = 7.55e-5 # 1/(A^2 s)
H = 0.016 # m

def f(x,u,t):
    xd = S.np.zeros(2)
    x1 = x[0]
    x2 = x[1]
    x3 = H
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    xd[0] = (1/L)*(u1-rho*x1*(x3-x2)/A-u_ac-E*x2-(R_arc+R)*x1)
    xd[1] = k1*x1+k2*x1*x1*(x3-x2)-u2-u3
    return xd

def g(x,u,t):
    return x

def e(x,u,t):
    return S.ResetResult(False,None)

def jac(x,u,t):
    x1 = x[0]
    x2 = x[1]
    x3 = H
    u1 = u[0]
    u2 = u[1]
    u3 = u[2]
    j = S.np.zeros([2,2])
    j[0,0] = (1/L)*(-rho*(x3-x2)/A-(R_arc+R))
    j[0,1] = (1/L)*(rho*x1/A - E)
    j[1,0] = k1+2*k2*x1*(x3-x2)
    j[1,1] = -k2*x1*x1
    return j
