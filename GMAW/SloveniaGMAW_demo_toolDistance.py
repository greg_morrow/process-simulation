import Solver as S
import Slovenia_GMAW as model
import matplotlib.pyplot as plt
import math

current = 10.0 # A
stick_out = 0.005 # m
w_1 = 0.0 # m
w_2 = 0.0 # m
voltage = 24.0 # V
torch_speed = 0.0083 # m/s
electrode_workpiece_distance = 0.016 # m

solver_dt = 1e-5

# 0 -> Voltage
# 1 -> torch speed
# 2 -> electrode to workpiece distance
def inputs(t, y):
    inp = [0,0,0,0]
    inp[0] = 24 # V
    ti = int(0.25*t)
    inp[1] = .0083 #+ ti*0.1 # m/s
    inp[2] = 0.010 + 0.020*ti # m
    inp[3] = 0.01
    return inp

x0 = [current, stick_out, w_1, w_2]
u0 = inputs(0, [])

s = S.Solver(x0,u0,0.0,solver_dt,model,inputs)
res = s.Simulate(10)

# create plots
title = 'Bead width with changing tool to workpiece distance'
#plt.title(title)
f = plt.figure(figsize=[13,6])
f.suptitle(title)
plt.subplot(2,2,1)
plt.plot(res.T, [y[2] for y in res.Y])
plt.ylabel('bead width (m)')
plt.subplot(2,2,3)
plt.plot(res.T, [y[3] for y in res.Y])
plt.xlabel('time (s)')
plt.ylabel('tool dist. (m)')
#plt.subplot(2,2,4)
#plt.plot(res.T, [y[0] for y in res.Y])
#plt.xlabel('time (s)')
#plt.ylabel('current (A)')
plt.subplot(2,2,2)
# create visualization of weld geometry
w = []
t = []
yy = [y[2] for y in res.Y]
v = [y[4] for y in res.Y]
i = 0
# for each y value, make two points, {res.T[i],-y/2} and {res.T[i],y/2}
for y in yy:
    w.append(-y/2)
    w.append(y/2)
    if (i > 0):
        p += (res.T[i]-res.T[i-1])*v[i]
    else:
        p = 0.0
    t.append(p)
    t.append(p)
    i += 1
plt.plot(t, w)
plt.xlabel('position on workpiece (m)')
plt.ylabel('weld geometry (m)')
plt.ylim([-0.125,0.125])
plt.savefig(title+'.png')
plt.show()
