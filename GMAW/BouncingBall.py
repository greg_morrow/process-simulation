# ODE model of a bouncing ball.
# The model has a reset condition that models the "bounce."
# When the reset occurs, the ball is placed slightly above
# the ground with the negative of its velocity multiplied by
# the restitution coefficient.
import Solver as S
import numpy as np

def f(x,u,t):
    return [x[1], -9.8]

def g(x,u,t):
    return x

def e(x,x_old,t):
    thresh = 0.0
    restitution = 0.6
    above = 1e-12
    if ((x[0]-thresh)*(x_old[0]-thresh)<=0):
        result = True
    else:
        result = False

    return S.ResetResult(result, [above,-restitution*x[1]])

def jac(x,u,t):
    j = np.zeros([2,2])
    j[0,1] = 1.0
    return j
    
