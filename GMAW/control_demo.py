import Solver as S
import first_order as model1
import second_order as model2
import matplotlib.pyplot as P
import Control as C

pi = C.PI_Controller(5,3,1000,-10)

def input(t,y):
    if (t > 5):
        return [0]
    else:
        return [pi.control(y[0], 3, t)]

t0 = 0
dt = 0.01
x0 = [0,0]
u0 = [0]
s = S.Solver(x0, u0, t0, dt, model2, input)

r = s.Simulate(10)

P.plot(r.T, r.Y)
P.show()
