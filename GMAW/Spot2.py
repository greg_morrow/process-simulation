# model spot welding as the thermal variation of a fixed volume of the material
# input heat is from I^2 R heating. Energy outputs are heat of fusion and difusion
import Solver as S
import math

T_0 = 300
T_melt = 1450
k = -100
k_R = 0.1

# specific heat as a function of temp
def Cp(T):
    C_nom = 10
    dT = 1
    H = 1000
    if (T < T_melt-dT or T > T_melt+dT):
        return C_nom
    else:
        return H/dT

# resistance as a function of temp
def R(T):
    R_0 = 100
    return R_0 + (T-T_0)*k_R

def f(x,u,t):
    T = x[0]
    return [((T-T_0)*k + u[0]*R(T))/Cp(T)]

def g(x,u,t):
    return [x[0], u[0]]

def e(x,u,t):
    return S.ResetResult(False, [])

def jac(x,u,t):
    return []
