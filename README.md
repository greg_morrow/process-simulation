Files related to the project "Process Simulation in KUKA.Sim."

See the overview powerpoint presentation "Documentation/process-simulation-overview.pptx"

To run the python files outside of KUKA.sim, you need python. You also need numpy and matplotlib.

To run the PDE files, you need FreeFem++.
