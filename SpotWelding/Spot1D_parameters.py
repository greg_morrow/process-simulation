# Models a parameter that is linear except for a discontinuity at a critical temperature.
# The linear portions can have different slopes.
# When evaluating the Value of the parameter, one provides the temperature as well as
# a liquid fraction (beta).
class BilinearParameter:
    def __init__(self, p_solid, slope_solid, p_liquid, slope_liquid, T_critical):
        self.p_s = p_solid
        self.s_s = slope_solid
        self.p_l = p_liquid
        self.s_l = slope_liquid
        self.Tc = T_critical

    def Value(self,T,beta):
        dT = T-self.Tc
        if (dT < 0):
            # solid
            return self.p_s + dT*self.s_s
        if (dT > 0):
            # liquid
            return self.p_l + dT*self.s_l
        # mushy zone
        return beta*self.p_l + (1-beta)*self.p_s



# Parameters for the Spot1D model. Default values from steel.
class Spot1D_Parameters:
    def __init__(self, N=300, n1 = 150, l1=0.002, l2=0.002, T_melt=1450.0, T_ambient=300.0, lambda_=2.6e5, V_0=20e3, t_V_end=0.265,R_0=1e-7, R_g_0=75.0,rho_s=8.06e3,rho_l=7e3,C=BilinearParameter(681,0,753,0,1450),K=BilinearParameter(1.1,0,0.9,0,1450),Sigma=BilinearParameter(10,0,10,0,1450)):
        self.N = N                  # number of elements
        self.n1 = n1                # number of elements in plate 1
        self.l1 = l1                # thickness of plate 1 (m)
        self.l2 = l2                # thickness of plate 2 (m)
        self.dx1 = l1/(n1-1)        # element size in plate 1 (m)
        self.dx2 = l2/(N-n1)        # element size in plate 2 (m)
        self.T_melt = T_melt        # melting temperature (K)
        self.T_ambient = T_ambient  # ambient temperature (K)
        self.lambda_ = lambda_      # heat of fusion (J)
        self.rho_s = rho_s          # solid density (kg/m^3)
        self.rho_l = rho_l          # liquid density (kg/m^3)
        self.C = C                  # specific heat of workpiece (J/K)
        self.K = K                  # thermal diffusivityof workpiece (J/(kg K))
        self.Sigma = Sigma          # resistivity of workpiece (Mho)
        self.R_0 = R_0              # resistance oif external circuit (Ohm)
        self.R_g_0 = R_g_0          # initial gap resistance (Ohm)
        self.k_R_g = -R_g_0/(T_melt-T_ambient) # gap resistance coefficient (Ohm/K)
        self.V_0 = V_0              # open-circuit voltage (V)
        self.t_V_end = t_V_end      # time at which to shut off voltage

    def set_l1(self, l1):
        self.l1 = l1
        self.dx1 = l1/(self.n1-1)
    def set_l2(self, l2):
        self.l2 = l2
        self.dx2 = l2/(self.N-self.n1)

class Spot1D_Solver_Parameters:
    def __init__(self, dt=0.0002, t_final = 0.5, saveTime = 0.01, saveTimeMin = 0.0, saveFig = True, verbose = False):
        self.dt = dt                # the time step
        self.t_final = t_final      # the final time (simulation starts t t = 0)
        self.verbose = verbose      # print some values during simulation
        self.setSaveInfo(saveTime, saveTimeMin, saveFig)

    def setSaveInfo(self, saveTime, saveTimeMin = 0.0, saveFig = False):
        self.saveTime = saveTime                # how often to save states during simulation
        self.saveTimeMin = saveTimeMin          # do not save anything before this time
        self.saveFig = saveFig                  # save any figures created
        self.saveRate = int(saveTime/self.dt)   # how many steps to skip between saves
        self.saveMin = int(saveTimeMin/self.dt) # minimum save time in steps

# physical parameters for Steel
Steel = Spot1D_Parameters()
