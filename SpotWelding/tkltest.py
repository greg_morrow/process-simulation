import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sys
if sys.version_info[0] < 3:
    import Tkinter as tk
else:
    import tkinter as tk
import matplotlib.backends.tkagg as tkagg
from matplotlib.backends.backend_agg import FigureCanvasAgg

def readDataFromFile(filename):
    file = open(filename,"r")
    l = file.readline()
    Data = []
    while l != "":
        nl = l.strip()
        nl = nl[1:len(nl)-1]
        ls = nl.split(",")
        a = []
        for l in ls:
            a.append(float(l))
        l = file.readline()
        Data.append(a)
    file.close()
    return Data


def draw_figure(canvas, figure, loc=(0, 0)):
    """ Draw a matplotlib figure onto a Tk canvas

    loc: location of top-left corner of figure on canvas in pixels.
    Inspired by matplotlib source: lib/matplotlib/backends/backend_tkagg.py
    """
    figure_canvas_agg = FigureCanvasAgg(figure)
    figure_canvas_agg.draw()
    figure_x, figure_y, figure_w, figure_h = figure.bbox.bounds
    figure_w, figure_h = int(figure_w), int(figure_h)
    photo = tk.PhotoImage(master=canvas, width=figure_w, height=figure_h)

    # Position: convert from top-left anchor to center anchor
    canvas.create_image(loc[0] + figure_w/2, loc[1] + figure_h/2, image=photo)

    # Unfortunately, there's no accessor for the pointer to the native renderer
    tkagg.blit(photo, figure_canvas_agg.get_renderer()._renderer, colormode=2)

    # Return a handle which contains a reference to the photo object
    # which must be kept live or else the picture disappears
    return photo

# Create a canvas
w, h = 1300, 800
window = tk.Tk()
window.title("Spot weld temperature history")
canvas = tk.Canvas(window, width=w, height=h)
canvas.pack()

# Generate some example data
X = np.linspace(0, 2 * np.pi, 50)
Y = np.sin(X)

Temps = readDataFromFile("temperatures.csv")
Nugget = readDataFromFile("nugget.csv")


x = range(0,300)
# Create the figure we desire to add to an existing canvas
fig = mpl.figure.Figure(figsize=(10, 7))
ax = fig.add_axes([0, 0, 1, 1])
ax.plot(x,map(list,zip(*Temps)))

# Keep this handle alive, or else figure will disappear
fig_x, fig_y = 50, 50
fig_photo = draw_figure(canvas, fig, loc=(fig_x, fig_y))
fig_w, fig_h = fig_photo.width(), fig_photo.height()

# Let Tk take over
tk.mainloop()
