import matplotlib.pyplot as P

f,a = P.subplots(2, sharex=True)
x = range(10)
y1 = [xx*xx for xx in x]
y2 = [xx*xx*xx for xx in x]
a[0].plot(x,y1)
a[1].plot(x,y2)
P.show()
