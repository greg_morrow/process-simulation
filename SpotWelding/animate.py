import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

file = open("temperatures.csv","r")

l = file.readline()

As = []

while l != "":
    nl = l.strip()
    nl = nl[1:len(nl)-1]
    ls = nl.split(",")
    a = []
    for l in ls:
        a.append(float(l))
    l = file.readline()
    As.append(a)
file.close()

x = range(len(As[0]))
fig, ax = plt.subplots()
ax.autoscale()
xdata, ydata = [], []
ln, = plt.plot(x, As[0], animated=True)

def init():
    ax.set_xlim(0, len(As[0]))
    ax.set_ylim(275,3000)
    return ln,

def update(frame):
    print "Update {}".format(frame)
    #ln.set_data(x, Ts[frame])
    #x = range(100)
    #y = []
    #for xx in x:
    #    y.append(frame*xx)
    ln.set_ydata(As[frame])
    return ln,

ani = FuncAnimation(fig, update, frames=range(len(As)), init_func=init, blit=True, interval = 1)
plt.show()
